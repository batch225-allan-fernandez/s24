/*3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
5. Create a variable address with a value of an array containing details of an address.
6. Destructure the array and print out a message with the full address using Template Literals.
7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.
9. Create an array of numbers.
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.*/

const getCube = Math.pow(2,3);
console.log(`The cube of 2 is ${getCube}`)

// ==============================================
let address = ["258", "Washington Ave NW", "California", "90011"]
let [streetNumber,streetName, state, zipCode] = address
console.log(`I live at ${streetNumber} ${streetName}, ${state} ${zipCode}`)

// ==============================================
const animal = {
	animalName:"Lolong",
	animalType:"saltwater Crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

let{animalName, animalType, weight, measurement} = animal
console.log(`${animalName} was a ${animalType}. He weighed at ${weight} with a measurement of ${measurement}.`)


// ==============================================
const givenNumber = [1,2,3,4,5]
givenNumber.forEach(function (givenNumber) {
	console.log(givenNumber)
})

const add = (n1, n2, n3, n4, n5) => n1+n2+n3+n4+n5
let total = add(1,2,3,4,5);
console.log(total)

// ==============================================
// or Reduce Array Method

function reduceNumber(accumulator, currentValue) {
  return accumulator + currentValue;
}

let sum = givenNumber.reduce(reduceNumber);
console.log(sum);


// ==============================================
class Dog {
	constructor(name, age, breed){
		this.name = name,
		this.age = age,
		this.breed = breed
	}
}

const myDog = new Dog ()
	myDog.name = "Frankie"
	myDog.age = 5
	myDog.breed = "Miniature Dachshund"

console.log(myDog)
